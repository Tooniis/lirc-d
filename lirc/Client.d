module lirc.Client;

/* TODO: Add OO wrapper */

extern(C) {
	struct lirc_config;

	int lirc_init(const char *prog, int verbose);
	int lirc_deinit();

	alias Callback = int function(char *);
	int lirc_readconfig (const char *path, lirc_config **config, Callback check);

	int lirc_send_one(int fd, const char * remote, const char * keysym);

	/* TODO: Add remaining declarations */
}
