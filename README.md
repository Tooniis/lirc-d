# D binding for LIRC

For now, this is meant to be integrated into projects as a submodule.

Only part of the [lirc_client API](https://lirc.org/api-docs/html/group__lirc__client.html) is supported at the moment.
